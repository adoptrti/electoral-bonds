# Aditya Birla Group

![Vi](https://media.assettype.com/newslaundry%2F2024-04%2F3d3d7368-8310-42e6-8068-1e4e92c5a25b%2FBonds4.png?auto=format%2Ccompress&fit=max&format=webp&w=1200&dpr=2.0)

## News and Reports

1. [Aditya Birla group donated Rs 100 crore to BJP two months before government’s Vodafone Idea bailout](https://www.newslaundry.com/2024/04/05/aditya-birla-group-donated-rs-100-crore-to-bjp-two-months-before-governments-vodafone-idea-bailout)
1. [Electoral Bonds Exclusive: 7 Firms Were Among BJP's Top Donors Over Last 5 Years](https://www.thequint.com/news/politics/electoral-bonds-bjp-top-companies-donations-election-commission-sbi)

## Other Names

1. ESSEL MINING AND INDS LTD (40)
2. ABNL INVESTMENT LIMITED (371)
3. GRASIM INDUSTRIES LTD (612)
4. BIRLA CARBON INDIA PRIVATE LIMITED (370)
5. UTKAL ALUMINA INTERNATIONAL LIMITED (369,673)
