# TORRENT POWER LIMITED (238)

![Torrent Power](https://www.torrentpower.com/images/logo.png)

## News and Reports

1. [Seven firms that failed drug quality tests gave money to political parties through electoral bonds
](https://scroll.in/article/1065318/seven-firms-that-failed-drug-quality-test-gave-money-to-political-parties-through-electoral-bonds#:~:text=Thirty%2Dfive%20pharmaceutical%20companies%20in,when%20they%20purchased%20the%20bonds.)
1. [Torrent Group buys Rs 184 crore electoral bonds, lion’s share of Rs 137 crore goes to BJP](https://www.newslaundry.com/2024/03/22/torrent-group-buys-rs-184-crore-electoral-bonds-lions-share-of-rs-137-crore-goes-to-bjp) - newslaundry.com

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/iOjthKqDlFg?si=5B-D8m8g3ZwZxUXg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</figure>


## Other names
1. TORRENT PHARMACEUTICALS LIMITED (493)
1. TORRENT PHARMACEUTICALS LTD (242)
1. TORRENT POWER LIMITED (238)
1. TORRENT POWER LTD (548)


