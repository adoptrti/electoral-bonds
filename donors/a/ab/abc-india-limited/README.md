# ABC INDIA LIMITED (Adani Group)

![Adani](https://media.assettype.com/newslaundry%2F2024-03%2F0a628752-2443-4f31-bea8-9f40c51986da%2Fadani_logo_ai_.jpg?auto=format%2Ccompress&fit=max&format=webp&w=1200&dpr=2.0)

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/CS0IUb4qdAk?si=AsEDWd3CfZwY_MgI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</figure>

## News and Reports

1. [These firms linked to Adani Group bought electoral bonds worth Rs 55.4 cr, BJP encashed Rs 42.4 cr](https://media.assettype.com/newslaundry%2F2024-03%2F0a628752-2443-4f31-bea8-9f40c51986da%2Fadani_logo_ai_.jpg?auto=format%2Ccompress&fit=max&format=webp&w=1200&dpr=2.0)
1. [Electoral Bonds: Gujarat Dalit Farmer 'Tricked' Into Donating Rs 10 Crore to BJP](https://www.thequint.com/news/politics/electoral-bonds-scam-dalit-farmer-says-tricked-into-donating-rs-11-crore-to-bjp-shiv-sena)