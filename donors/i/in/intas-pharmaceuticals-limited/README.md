# INTAS PHARMACEUTICALS LIMITED (544)

## News and Reports

1. [Seven firms that failed drug quality tests gave money to political parties through electoral bonds
](https://scroll.in/article/1065318/seven-firms-that-failed-drug-quality-test-gave-money-to-political-parties-through-electoral-bonds#:~:text=Thirty%2Dfive%20pharmaceutical%20companies%20in,when%20they%20purchased%20the%20bonds.)

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/iOjthKqDlFg?si=5B-D8m8g3ZwZxUXg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</figure>
