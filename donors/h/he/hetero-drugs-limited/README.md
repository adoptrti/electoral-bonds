# HETERO Group (356)

![](https://media.assettype.com/newslaundry%2F2024-03%2F6a992602-cb29-4614-8797-122083044e77%2FAI_RESIZE.jpg?auto=format%2Ccompress&fit=max&format=webp&w=1200&dpr=2.0)

## News and Repoorts

1. [Seven firms that failed drug quality tests gave money to political parties through electoral bonds
](https://scroll.in/article/1065318/seven-firms-that-failed-drug-quality-test-gave-money-to-political-parties-through-electoral-bonds#:~:text=Thirty%2Dfive%20pharmaceutical%20companies%20in,when%20they%20purchased%20the%20bonds.)
1. [A Rajya Sabha nomination and bonds worth Rs 120 crore to BRS](https://www.newslaundry.com/2024/03/26/a-rajya-sabha-nomination-and-bonds-worth-rs-120-crore-to-brs-at-the-time)

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/iOjthKqDlFg?si=5B-D8m8g3ZwZxUXg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</figure>

## Other names
1. HETERO BIOPHARMA LIMITED (422)
1. HETERO DRUGS LIMITED (356)
1. HETERO LABS LIMITED (424)
1. HINDYS LAB PRIVATE LTD (154)
1. SELMAR LAB PRIVATE LTD (153)
1. HONOUR LAB LIMITED (220)
1. HONOUR LAB LTD (357)
1. HAZELO LAB PRIVATE LTD (152)
1. DASAMI LAB PRIVATE LIMITED (FORMERLY HEL IUS LAB PVT LTD) (423)
1. DASAMI LAB PRIVATE LTD (155)
1. DANIKA TRADERS PRIVATE LTD (157)



